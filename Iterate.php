<?php
require './vendor/autoload.php';
date_default_timezone_set('Europe/London');

// Store the micro time so that we know when our script started to run.
$executionStartTime = microtime(true);

// Initialise: Dotenv\Dotenv
$dotenv = new Dotenv\Dotenv(__DIR__);
$dotenv->load();

// Initialise: Box\Spout
use Box\Spout\Reader\ReaderFactory;
use Box\Spout\Writer\WriterFactory;
use Box\Spout\Common\Type;
use Dotenv\Dotenv;

$spoutReader = ReaderFactory::create(Type::CSV);
$spoutWriter = WriterFactory::create(Type::CSV);

// Load options
$charset = explode(',', getenv('CHARSET'));
$length = getenv('LENGTH');
$output_dir = getenv('OUTPUT_DIR');

// Perform Iteration
for ($i=0; $i < $length; $i++) {
    if ($i == 0) {
        $spoutWriter->openToFile($output_dir . ($i+1) . ".csv");
        foreach ($charset as $char) {
            $totalGenerated++;
            echo "[" . number_format($totalGenerated) . "] $char\n";
            $spoutWriter->addRow(array($char));
        }
        $spoutWriter->close();
    } else {
        $spoutReader->open("$output_dir$i.csv");
        $spoutWriter->openToFile($output_dir . ($i+1) . ".csv");
        foreach ($spoutReader->getSheetIterator() as $sheet) {
            foreach ($sheet->getRowIterator() as $row) {
                foreach ($row as $cell) {
                    foreach ($charset as $char) {
                        $domain = $char . $cell;
                        $totalGenerated++;
                        echo "[" . number_format($totalGenerated) . "] $domain\n";
                        $spoutWriter->addRow(array($domain));
                    }
                }
            }
        }
        $spoutReader->close();
        $spoutWriter->close();
    }

    echo "\n\n*** " . number_format($totalGenerated) . " combinations created in " . round((microtime(true) - $executionStartTime), 3) . " seconds! ***\n\n";
}
